package com.battletech.maddog.socket;

public interface MessageHandler <T,R> {
    R onMessageReceived(R messageReceived, ClientInstance<T> socketClientInstance);
}
