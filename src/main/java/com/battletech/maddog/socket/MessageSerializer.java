package com.battletech.maddog.socket;

import java.util.Optional;

public interface MessageSerializer <T,R> {
    R readAsObject(String message) throws Exception;
    String writeAsString(T message) throws Exception;
    String getReaderDelimiter();
    Optional<String> getWriterDelimiter();
}
