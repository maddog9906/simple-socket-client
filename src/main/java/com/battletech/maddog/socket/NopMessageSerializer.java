package com.battletech.maddog.socket;

import java.security.cert.CRL;
import java.util.Optional;

public class NopMessageSerializer implements MessageSerializer<String, String> {

    private static final String CRLF = "\r\n";

    @Override
    public String readAsObject(String message) throws Exception {
        return message;
    }

    @Override
    public String writeAsString(String message) throws Exception {
        return message;
    }

    @Override
    public String getReaderDelimiter() {
        return CRLF;
    }

    @Override
    public Optional<String> getWriterDelimiter() {
        return Optional.of(CRLF);
    }
}
