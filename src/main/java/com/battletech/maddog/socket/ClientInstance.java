package com.battletech.maddog.socket;

public interface ClientInstance<T> {
    void reConnect();
    void stop();
    void sendMessage(T message);
    boolean isConnected();
}
