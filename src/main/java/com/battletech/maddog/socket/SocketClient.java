package com.battletech.maddog.socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class SocketClient <T,R> implements ClientInstance<T> {
    private static Logger LOG = LoggerFactory.getLogger(SocketClient.class);

    private final ClientConfig config;
    private final MessageSerializer<T,R> messageSerializer;
    private final SSLContext sslContext;

    private Timer timer = null;
    private Long lastReceivedTime = null;
    private Socket socket = null;
    private BufferedWriter writer = null;
    private Scanner scanner = null;
    private AtomicBoolean connected = new AtomicBoolean(false);
    private AtomicBoolean threadStarted = new AtomicBoolean(false);
    private AtomicBoolean shouldConnect = new AtomicBoolean(false);
    private List<MessageHandler<T,R>> messageHandlers = new ArrayList<>();

    public SocketClient(ClientConfig config,
                        MessageSerializer<T,R> messageSerializer) {
        this(config, messageSerializer, null);
    }

    public SocketClient(ClientConfig config,
                        MessageSerializer<T,R> messageSerializer,
                        SSLContext sslContext) {
        this.config = config;
        this.messageSerializer = messageSerializer;
        this.sslContext = sslContext;
    }

    public void start() {
        this.shouldConnect.set(true);
        timer = new Timer();
        timer.schedule(new RefreshAndValidateConnection(), 1_000);
    }

    @Override
    public void stop() {
        if (timer!=null) {
            timer.cancel();
        }
        this.shouldConnect.set(false);
        disconnect();
    }

    @Override
    public void reConnect() {
        stop();
        start();
    }

    @Override
    public void sendMessage(T message) {
        try {
            onSendMessage(message, writer);
        }
        catch (IOException e) {
            LOG.error("Socket Client: Error sending to socket - disconnecting", e);
            writer = null;
            disconnect();
        }
    }

    @Override
    public boolean isConnected() {
        return connected.get();
    }

    public void addMessageHandler(MessageHandler<T,R> handler) {
        this.messageHandlers.add(handler);
    }

    private void onMessageReceived(String message) {
        R msgObj;
        try{
            msgObj = messageSerializer.readAsObject(message);
        } catch (Exception e) {
            LOG.error("Streaming Client: Error converting message. This message will be ignored...", e);
            return;
        }

        for (MessageHandler<T, R> handler : messageHandlers) {
            try {
                msgObj = handler.onMessageReceived(msgObj, this);
            } catch (Exception e) {
                LOG.error("Streaming Client: Error in messageHandler!!!!", e);
            }
        }
    }

    private void onSendMessage(T message, Writer writer) throws IOException {
        try {
            String encodedMessageToSend = messageSerializer.writeAsString(message);
            LOG.trace("Socket Client: Writing message {}", encodedMessageToSend);
            // Write to output stream
            writer.write(encodedMessageToSend);
            writer.write(messageSerializer.getWriterDelimiter().orElse(""));
            writer.flush();
        } catch (Exception e) {
            LOG.error("Socket Client: Error sending message", e);
        }
    }

    private Socket createSSLSocket() throws IOException {
        LOG.info( "Socket Client: Trying to connect " + config.getServerHostname() + ":" + config.getServerPort());
        SocketFactory factory = sslContext==null? SSLSocketFactory.getDefault() : sslContext.getSocketFactory();
        SSLSocket newSocket = (SSLSocket) factory.createSocket(config.getServerHostname(), config.getServerPort());
        newSocket.startHandshake();
        return newSocket;
    }

    private synchronized void connect() {
        try {
            // Create Socket
            if (config.getServerPort() == 443 || config.isSsl()) {
                socket = createSSLSocket();
            } else {
                socket = new Socket(config.getServerHostname(), config.getServerPort());
            }
            socket.setReceiveBufferSize(config.getReceiveBufferSize());
            scanner = new Scanner(socket.getInputStream());
            scanner.useDelimiter(messageSerializer.getReaderDelimiter());
            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            connected.set(true);
            // Start Processing Thread
            Thread thread = new Thread(messageReader);
            thread.start();

        } catch (UnknownHostException e) {
            LOG.error ("Socket Client: Exception due to Unknown host", e);
        } catch (IOException e) {
            LOG.error ("Socket Client: Exception due to IO", e);
        } catch (Exception e) {
            LOG.error ("Socket Client: Exception", e);
        }
    }

    private synchronized void disconnect() {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                LOG.warn("Socket Client: Unable to close socket", e);
            } finally {
                connected.set(false);
                lastReceivedTime = null;
                socket = null;
            }
        }
    }

    private Runnable messageReader = () -> {
        threadStarted.set(true);
        LOG.info("Socket Client: Read Messages thread started");
        while (connected.get()) {
            String line;
            try {
                line = scanner.next();
                LOG.trace("Socket Client: Receiving message {}", line);
                lastReceivedTime = System.currentTimeMillis();
                onMessageReceived(line);
            }
            catch (NoSuchElementException e) {
                LOG.error("Socket Client: EOF detected - disconnecting");
                scanner.close();
                scanner = null;
                disconnect();
            }
            catch (Exception e) {
                LOG.error("Socket Client: Error received processing socket - disconnecting:", e);
                scanner.close();
                scanner = null;
                disconnect();
            }
        }
        threadStarted.set(false);
        LOG.warn("Socket: Read Messages thread stopped");
    };

    private class RefreshAndValidateConnection extends TimerTask {
        @Override
        public void run() {
            if (shouldConnect.get() && (!connected.get() && !threadStarted.get())) {
                connect();
            }

            if (config.getReconnectTimeoutSeconds() >0 && connected.get() && lastReceivedTime !=null &&
                    System.currentTimeMillis() - lastReceivedTime > (config.getReconnectTimeoutSeconds() * 1000)) {
                LOG.warn("Socket Client: No messages received for more than " + config.getReconnectTimeoutSeconds() + " secs... Reconnecting....");
                disconnect();
            }
        }
    }
}
