package com.battletech.maddog.socket;

public class ClientConfig {
    private static final int DEFAULT_RECEIVE_BUFFER_SIZE = 1024 * 1024 * 2;

    private String serverHostname;
    private int serverPort;
    private boolean ssl;
    private int receiveBufferSize;
    private int reconnectTimeoutSeconds;

    private ClientConfig() {}

    public String getServerHostname() {
        return serverHostname;
    }

    public int getServerPort() {
        return serverPort;
    }

    public boolean isSsl() {
        return ssl;
    }

    public int getReceiveBufferSize() {
        return receiveBufferSize;
    }

    public int getReconnectTimeoutSeconds() {
        return reconnectTimeoutSeconds;
    }

    public static class Builder {
        private String serverHostname;
        private int serverPort;
        private boolean ssl;
        private int receiveBufferSize;
        private int reconnectTimeoutSeconds;

        public Builder serverHostname(String serverHostname){
            this.serverHostname = serverHostname;
            return this;
        }

        public Builder serverPort(int serverPort){
            this.serverPort = serverPort;
            return this;
        }

        public Builder ssl(boolean ssl){
            this.ssl = ssl;
            return this;
        }

        public Builder receiveBufferSize(int receiveBufferSize){
            this.receiveBufferSize = receiveBufferSize;
            return this;
        }

        public Builder reconnectTimeoutSeconds(int reconnectTimeoutSeconds){
            this.reconnectTimeoutSeconds = reconnectTimeoutSeconds;
            return this;
        }

        public ClientConfig build(){
            ClientConfig config = new ClientConfig();
            config.serverHostname = this.serverHostname;
            config.serverPort = this.serverPort;
            config.ssl = this.ssl;
            config.reconnectTimeoutSeconds = this.reconnectTimeoutSeconds;
            config.receiveBufferSize = this.receiveBufferSize;
            if (config.receiveBufferSize<=0) {
                config.receiveBufferSize = DEFAULT_RECEIVE_BUFFER_SIZE;
            }
            return config;
        }
    }
}
