package com.battletech.maddog.socket

import spock.lang.Specification
import spock.lang.Subject

class SocketClientTest extends Specification {

    EchoSocketServer server

    @Subject
    SocketClient<String, String> socketClient

    def setup(){
        server = new EchoSocketServer()
        server.start(1244)
    }

    def cleanup(){
        server.stop()
        server = null
    }

    def "test socket client"() {
        given:
        def config = new ClientConfig.Builder()
                            .serverHostname("localhost")
                            .serverPort(1244)
                            .build()

        MessageHandler<String,String> handler = new MessageHandler<String, String>() {
            List<String> messageBuffer = []

            @Override
            String onMessageReceived(String messageReceived, ClientInstance<String> socketClientInstance) {
                if (messageReceived == "ONE") {
                    socketClientInstance.sendMessage("TWO")
                    messageBuffer << messageReceived
                } else if (messageReceived == "TWO") {
                    socketClientInstance.sendMessage("THREE")
                    messageBuffer << messageReceived

                } else if (messageReceived == "THREE") {
                    socketClientInstance.sendMessage(".")
                    messageBuffer << messageReceived
                }
                messageReceived
            }
        }

        socketClient = new SocketClient(config, new NopMessageSerializer<>())
        socketClient.addMessageHandler(handler)
        socketClient.start()
        while (!socketClient.isConnected()) {
            Thread.sleep(200)
        }

        when:
        socketClient.sendMessage("ONE")
        Thread.sleep(1_000)

        then:
        assert handler.messageBuffer.size() == 3
        assert handler.messageBuffer == ["ONE","TWO","THREE"]

        cleanup:
        socketClient.stop()
    }
}
