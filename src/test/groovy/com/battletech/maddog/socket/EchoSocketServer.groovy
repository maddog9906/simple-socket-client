package com.battletech.maddog.socket

class EchoSocketServer {
    ServerSocket serverSocket
    Socket clientSocket
    PrintWriter writer
    BufferedReader reader

    void start(int port) {
        Thread.start {
            serverSocket = new ServerSocket(port)
            clientSocket = serverSocket.accept()
            writer = new PrintWriter(clientSocket.getOutputStream())
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))

            String inputLine
            while ((inputLine = reader.readLine()) != null) {
                if ("." == inputLine) {
                    writer.write("good bye")
                    writer.write("\r\n")
                    writer.flush()
                    break
                }
                writer.write(inputLine)
                writer.write("\r\n")
                writer.flush()
            }
        }

    }

    void stop() {
        reader.close()
        writer.close()
        clientSocket.close()
        serverSocket.close()
    }

}
