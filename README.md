# Simple Socket Client

## What is simple-socket-client
`simple-socket-client` as the name implies is simply, a socket client :)

## Declaring the dependency
```Groovy
    repositories {
        ...
        maven { url 'https://raw.githubusercontent.com/alvin9906/maven-repo/master' }  // maven-repo at github
        ...
    }    
     
   dependencies {
        .....
        compile 'com.battletech.maddog:socket-client:0.1' 
        .....
   }
```

## Usage
This supports only sending and receiving of String messages.

You will need to implement the `MessageSerializer` interface so that an `Object` can be 
converted to a `String` before writing to the socket and a message received can be
converted to another `Object` to be processed by 0 or more message handlers.

You will also need to implement at least 1 `MessageHandler` if you need to read or process
the received message.

To register the message handlers,
```
    socketClient.addMessageHandler(myHandler)
```
Multiple message handlers are supported, but do note that the handlers are executed
sequentially as according to the sequence of them added.

An instance of the socketClient is passed into the message handlers. 
So, it is possible for the message handler to write to the socket, stop the socket
or even re-establish the socket connection.

Please refer to the test module for example.


 

